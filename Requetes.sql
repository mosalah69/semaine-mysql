-- Inserer un user

CREATE PROCEDURE `register_user`(IN p_id int, IN p_firstName varchar(20),IN p_lastName Varchar(20), IN p_phone_number Int, IN p_gender Varchar(20), IN p_password Varchar(45), IN p_email Varchar(45) )
BEGIN

insert into user(id, firstName, lastName,phone_number,gender , password, email  ) values (p_id, p_firstName,p_lastName, p_phone_number, p_gender, p_password, p_email);

END

-- Inserer une adresse 

CREATE PROCEDURE `new_adresse`(IN p_id int, IN p_street varchar(20), IN p_streetNumber Int, IN p_postCode Int, IN p_city Varchar(45), IN p_country Varchar(45) )
BEGIN

insert into Address(id, street, streetNumber,postCode , city , country  ) values (p_id, p_street,p_streetNumber, p_postCode, p_city, p_country);

END

-- Inserer une activité

CREATE PROCEDURE `new_activity` (IN p_id int, IN p_title varchar(45), IN p_description VARCHAR(250), IN p_volunteers Int(10), IN p_woofing TINYINT(1), IN p_startdate Date, In p_endDate DATE, IN p_pendingApplication TINYINT(4), IN p_accomodation TINYINT(1), IN p_meal TINYINT(1) )
BEGIN

insert into Activity(id, title, description ,volunteers , woofing , startdate,  endDate , pendingApplication , accomodation , meal ) values (p_id, p_title, p_description , p_volunteers , p_woofing , p_startdate,  p_endDate , p_pendingApplication , p_accomodation , p_meal );
END

-- TRIGGER

CREATE TRIGGER ville_enmajuscule
BEFORE INSERT ON address FOR EACH ROW SET NEW.city =UPPER(NEW.city);
